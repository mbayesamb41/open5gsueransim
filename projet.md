**COMPTE RENDU SUR RESEAUX MOBILES 5G**
![](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.001.png)

![](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.002.png)![](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.003.png)










RÉPUBLIQUE DU SÉNÉGAL

\*\*\*\*\*\*\*\*\*\*\*\*\*\*

MINISTÈRE DE L’ENSEIGNEMENT SUPÉRIEUR ET DE LA RECHERCHE

SCIENTIFIQUE

\*\*\*\*\*\*\*\*\*\*\*\*\*

ÉCOLE CENTRALE DES LOGICIELS LIBRES ET DES TÉLÉCOMMUNICATIONS


![](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.004.png)



![](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.005.png)

`		`**COMPTE RENDU SUR RESEAUX MOBILES**


**Présenté par :							Encadrant :**

Mbaye Ndiaye SAMB						      Mr OUYA

Yacine Bigué SAMBE

Adris FERNANDO

Hilali SOUBIRA

Aboulai CAMARA

Yacin HAYAD				                	ANNEE ACADEMIQUE 2022



**SOMMAIRE**

**I-Objectif** 

**II- Etude sur 5G**

1-Historique 

2-architecture générale de 5G 

**III-Présentation de l’outil open5Gs et UERANSIM**

1-Architecture 

2-Open5Gs

`     `a-Définition 

`     `b-Architecture de Open5Gs

3- Définition de **UERANSIM**

**VI-Architecture du TP**

**V-Mise en place du réseau avec open5GS et UERANSIM**

1-Installation du cœur de réseau open5gs

a-Installation  open5Gs

b- Installation de open5gs avec les composants du cœur       

c-Configuration du Cœur de réseau Open5Gs 

2-Instalation de UERANSIM sur les deux clients et le gNodeB

`         `3-Configuration de gNodeB, client1 et client2

**Conclusion**






**I-Objectif** 

- Utiliser 3 machines : le cœur du réseau et 2 clients.

- Expliquer chaque paramètre et interpréter les résultats.

- Fournir un rapport complet dans gitlab

**II- Etude sur 5G**

1-Historique 

La **5G** est la **cinquième génération** des standards pour la [téléphonie mobile](https://fr.wikipedia.org/wiki/T%C3%A9l%C3%A9phonie_mobile "Téléphonie mobile"). Elle succède à la quatrième génération, appelée [4G](https://fr.wikipedia.org/wiki/LTE_Advanced "LTE Advanced"), en proposant des débits plus importants et une latence fortement réduite, tout en évitant le risque de saturation des réseaux lié à l'augmentation des usages numériques (smartphones, tablettes, [objets connectés](https://fr.wikipedia.org/wiki/Internet_des_objets "Internet des objets")). Son déploiement fait l'objet de contestations concernant en particulier l'[effet sanitaire des ondes électromagnétiques](https://fr.wikipedia.org/wiki/Effet_des_rayonnements_%C3%A9lectromagn%C3%A9tiques_sur_la_sant%C3%A9 "Effet des rayonnements électromagnétiques sur la santé") et l'[impact environnemental](https://fr.wikipedia.org/wiki/Impact_environnemental "Impact environnemental") de cette technologie.

2-architecture générale de 5G 

Le réseau central 5G, qui permet la fonctionnalité avancée des réseaux 5G, est l'un des trois principaux composants du **système 5G**, également appelé **5GS**. Les deux autres composants sont le **réseau d'accès 5G (5G-AN)** et l'**équipement utilisateur (UE)**. Le cœur de la 5G utilise une architecture basée sur les services (SBA) alignée sur le cloud pour prendre en charge l'authentification, la sécurité, la gestion des sessions et l'agrégation du trafic des appareils connectés, le tout nécessitant l'interconnexion complexe des fonctions du réseau, comme le montre le schéma du cœur de la 5G.

![Le réseau 5G – 5GS | Frédéric Launay](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.006.png)

Architecture du réseau 5G

Voici comment cela fonctionne :

- Les **équipements utilisateurs (UE** ), tels que les smartphones 5G ou les appareils cellulaires 5G, se connectent via le nouveau réseau d'accès radio 5G au cœur du réseau 5G, puis aux **réseaux de données (DN)**, comme l'internet.
- La **fonction de gestion de l'accès et de la mobilité (AMF)** fait office de point d'entrée unique pour la connexion de l'UE.
- En fonction du service demandé par l'UE, l'AMF sélectionne la fonction de gestion de session (SMF) respective pour gérer la session utilisateur.
- La **fonction de plan d'utilisateur (UPF)** transporte le trafic de données IP (plan d'utilisateur) entre l'équipement utilisateur (UE) et les réseaux externes.
- La **fonction de serveur d'authentification (AUSF** ) permet à l'AMF d'authentifier l'UE et d'accéder aux services du noyau 5G.
- D'autres fonctions telles que la **fonction de gestion de session (SMF)**, la **fonction de contrôle de politique (PCF**), la **fonction d'application (AF)** et la fonction de **gestion unifiée des données (UDM)** fournissent le cadre de contrôle de politique, en appliquant les décisions de politique et en accédant aux informations d'abonnement, pour régir le comportement du réseau.

**III-Présentation de l’outil open5Gs et UERANSIM**

![Sensors | Free Full-Text | Effective Feature Selection Methods to Detect  IoT DDoS Attack in 5G Core Network | HTML](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.007.png)1-Architecture 

Architecture de la communication entre UERANSIM et Open5Gs




2-Open5Gs

a-Définition 

Ce projet peut être utilisé pour configurer votre propre réseau NR/LTE. Si gNB/eNB et USIM sont disponibles, vous pouvez créer un réseau privé à l'aide d'Open5GS.

Open5GS a implémenté 5GC et EPC en utilisant le langage C. Et WebUI est fourni à des fins de test et est implémenté dans Node.JS et React.


b-Architecture de Open5Gs

![](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.008.png)

Architecture générale de Open5Gs

3- Définition de **UERANSIM**

**UERANSIM** est une source 5G UEet une 5G RAN(gNodeB)implémentation open source. Il peut être considéré comme un téléphone mobile 5G et une station de base en termes simples. Il existe 3 interfaces principales dans la perspective UE/RAN, 

- Control Interface (between RAN and AMF), 
- User Interface (between RAN and UPF), 
- Radio Interface (between UE and RAN). UERANSIM prend en charge l'exécution avec les réseaux Open5GS et Free5GC 5G Core. Nous pouvons connecter UERANSIM à l'un de ces réseaux 5G Core et tester la fonctionnalité.

**VI-Architecture du TP**

Pour ce TP il suffit d’avoir 4 machines **Ubuntu 18.04**

- Le cœur du réseau
- La partie Radio gNodeB
- Client 1
- Client 2

![](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.009.png)

Architecture du TP

**V-Mise en place du réseau avec open5GS et UERANSIM**

1-Installation du cœur de réseau open5gs

a-Installation  open5Gs

**# apt-get update**

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\4.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.010.png)

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\1.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.011.png)**# apt-get install make g++ libsctp-dev lksctp-tools iproute2** 



**#snap refresh core** 

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\Capture du 2022-05-23 11-02-40.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.012.png)

**# snap install cmake –classic**

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\21.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.013.png)

b- Installation de open5gs avec les composants du cœur de 5G SA

**#apt update** 

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\4.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.014.png)

**# apt install software-properties-common** 

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\5.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.015.png)

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\6.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.016.png)**#add-apt-repository ppa:open5gs/latest** 

**# apt update** 

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\4.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.017.png)

**# apt install open5gs**

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\8.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.018.png)

c-Configuration du Cœur de réseau Open5Gs 

Nous allons configurer les entités suivantes :

- **AMF** 

L’entité AMF (Access and Mobility Managmenent Function) reprend le rôle de l’entité MME. L’entité AMF établit une connexion NAS avec le mobile UE et a pour rôle d’enregistrer (attachement) les mobiles UE et de gérer la localisation des mobiles sur le réseau 3GPP et/ou non 3GPP.

Pour la configuration de AMF il suffit d’éditer le fichier **/etc/open5gs/amf.yaml** et définir l’adresse IP de l’interface NGAP (N2) qui relie le gNB et AMF

**@Ip ngap = @ip de la machine Open5G = 192.168.1.16/24**

MCC = le code du pays

MNC = le code de l’operateur



![G:\TPMercrediRX\Capture du 2022-07-03 12-19-11.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.019.png)

Redémarrage du service amfd et vérification du service  

**#/usr/bin/open5gs-amfd –c /etc/open5gs/amf.yaml**

![G:\TPMercrediRX\Capture du 2022-07-03 12-20-43.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.020.png)

Il faut ouvrir un autre terminal pour vérifier si le amf est bien démarré

**#netstat –anp | grep amf**

![G:\TPMercrediRX\Capture du 2022-07-03 12-22-26.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.021.png)

- **UPF**

La fonction de plan utilisateur (UPF) est un composant fondamental d'une architecture de système d'infrastructure centrale 3GPP 5G. L'UPF représente l'évolution du plan de données d'une stratégie de séparation du plan de contrôle et de l'utilisateur (CUPS), introduite pour la première fois en tant qu'extension des cœurs de paquets évolués (EPC) existants par le 3GPP dans ses spécifications de la version 14. CUPS découple les fonctions de contrôle et de plan utilisateur de la passerelle de paquets (PGW), permettant au composant de transfert de données (PGW-U) d'être décentralisé. Cela permet d'effectuer le traitement des paquets et l'agrégation du trafic plus près de la périphérie du réseau, ce qui augmente l'efficacité de la bande passante tout en réduisant le réseau. Le trafic de signalisation de gestion de la PGW (PGW-C) reste dans le noyau, en direction nord de l'entité de gestion de la mobilité (MME).

Pour la configuration il faut éditer le fichier **/etc/open5gs/upf.yaml** et définir l’adresse ip du protocole **gtpu** qui est utilisé par les interfaces **S1-U, X2, S4, S5 et S8** du système Evolved Packet System (EPS).

- **@ip de gtpu = @ip de la machine open5gs = 192.168.0.16**


**![G:\TPMercrediRX\Capture du 2022-07-03 12-23-37.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.022.png)**

Démarrons l’entité UPF

**#/usr/bin/open5gs-upfd –c /etc/open5gs/upf.yaml**

![G:\TPMercrediRX\Capture du 2022-07-03 12-25-06.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.023.png)

![G:\TPMercrediRX\Capture du 2022-07-03 12-25-41.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.024.png)Vérifions l’entité UPF est démarré par la commande suivante 

- Configuration de l’interface Web de Open5Gs 

L’interface Web permet de modifier de manière interactive les données des abonnés. Bien qu'il ne soit pas essentiel de l'utiliser, elle facilite les choses lorsque vous débutez dans l'aventure Open5GS. (Un outil en ligne de commande est disponible pour les utilisateurs avancés)

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\12.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.025.png)#**update**

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\13.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.026.png)**#apt install curl**	

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\14.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.027.png)#**curl -fsSL https://deb.nodesource.com/setup\_14.x | sudo -E bash -**





**#apt install nodejs**

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\15.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.028.png)

Installation de l’interface 

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\16.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.029.png)**#curl -fsSL https://open5gs.org/open5gs/assets/webui/install | sudo -E bash -**

Après l’installation, vous allez accéder à WebUI via http://192.168.1.66:3000 avec:

login: **admin** 

password: **1423**

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\17.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.030.png)









Création de compte pour les deux UERANSIM (client1 et client2)

Pour cela nous allons sur l’interface de open5G en mettant [**http://localhost:3000](http://localhost:3000)** et cliquer sur icone **+**

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\23.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.031.png)



Puis on renseigne le IMSI de l’abonné qui est dans l’EU 

![E:\OPEN5GS2\compte1.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.032.png)**IMSI = 901700000000001**

![](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.033.png)![E:\OPEN5GS2\compte2.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.034.png)Pour sauvegarder il suffit de cliquer sur **save** 



![G:\TPMercrediRX\Capture du 2022-07-03 13-38-47.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.035.png)

**Client1 : 901700000000001**

**Client2 : 901700000000004**

- Activation du routage de la machine open5gs

**#echo 1 > /proc/sys/net/ipv4/ip\_forward** 

**# sysctl –p**

**# iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o ogstun -j MASQUERADE** 

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\29.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.036.png)# **iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o enp0s3 -j MASQUERAD**


2-Instalation de UERANSIM sur les deux clients et le gNodeB

Le UERANSIM stimule la partie radio et l’UE. Donc nous allons installer le UERANSIM sur les trois machines (client1, client2 et le gNodeB). L’installation est la même sur les trois machines 

Les prérequis 

**#apt update![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\18.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.037.png)**

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\19.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.038.png)**#apt-get install make g++ libsctp-dev lksctp-tools iproute2**

**#snap refresh core![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\20.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.039.png)**

**# snap install cmake –classic![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\21.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.040.png)**

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\22.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.041.png)Téléchargement de UERANSIM par github

Compilation de UERANSIM 

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\24.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.042.png)**#make**

` `Sortie de la commande 

**cp cmake-build-release/nr-ue build/**

**cp cmake-build-release/nr-cli build/**

**cp cmake-build-release/libdevbnd.so build/**

**cp tools/nr-binder build/**

Nous obtenons des fichiers dans **UERANSIM/build** après avoir compilé

![C:\Users\MBAYE\OneDrive\Bureau\open5GSACaptures\25.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.043.png) 

3-Configuration de gNodeB, client1 et client2

- **gNodeB**

Pour configurer le UERANSIM, il faut définir les paramètres suivants dans le fichier **config/open5gs-gnb.yaml** 

- **linKip** = Adresse IP locale de gNodeB pour la simulation de liaison Radio =**192.168.1.14**
- **ngapIP =** Adresse IP de l’interface N2 qui relie le UERANSIM et le gNB=**192.168.1.4**
- **ntpIp =** Adresse IP de l’interface N3 = **192.168.1.14**
- L’adresse de **alfConfig =** adresse IP de la machine qu’on a configuré notre AMF Open5Gs = **192.168.1.16**

![G:\TPMercrediRX\Capture du 2022-07-03 12-30-16.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.044.png)

![G:\TPMercrediRX\Capture du 2022-07-03 12-32-01.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.045.png)Nous allons démarrer le service gNodeB depuis le répertoire UERANSIM en lançant le service nr-gnb avec le fichier de configuration que nous venons de configurer dans config/open5gs-gnb.yaml.

D’après la capture ci-dessus notre gNodeB est bien démarré

- Client 1

![G:\TPMercrediRX\Capture du 2022-07-03 12-48-45.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.046.png)Pour le client1 il suffit de préciser l’adresse IP du gNodeB 192.168.1.14 dans le fichier **UERANSIM/config/open5gs-ue.yaml**








Démarrons le client par la commande suivante 

![G:\TPMercrediRX\Capture du 2022-07-03 12-45-32.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.047.png)![](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.048.png)**#build/nr-ue –c config/open5gs-ue.yaml**

D’après la capture ci-dessus le client a 10.45.0.5 comme d adresse IP

Activation du routage et testons si l’ue parvient à accéder à la connexion internet

**#echo 1 > /proc/sys/net/ipv4/ip\_forward** 

**# sysctl –p**

**# iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o ogstun -j MASQUERADE** 

\# **iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o enp0s3 -j MASQUERADE**

#**ifconfig uesimtun0**

**#ping –I uesimtun0 8.8.8.8**

![G:\TPMercrediRX\Capture du 2022-07-03 13-06-41.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.049.png)

On peut aussi utiliser le navigateur firefox du client1 pour tester la connexion internet 

Pour ouvrir le navigateur fixefox on tape la commande suivante 

**#./nr-binder 10.45.0.5 firefox** 

![G:\TPMercrediRX\Capture du 2022-07-03 13-12-18.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.050.png)

Essayons de consulter le site de EC2LT à partir du navigateur de client1

![G:\TPMercrediRX\Capture du 2022-07-03 13-11-47.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.051.png) 

- Client 2

Pour le client1 il suffit de préciser l’adresse IP du gNodeB 192.168.1.14 dans le fichier **UERANSIM/config/open5gs-ue.yaml**

![G:\TPMercrediRX\Capture du 2022-07-03 12-47-35.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.052.png)

Démarrons le client2 par la commande suivante 

**#build/nr-ue –c config/open5gs-ue.yaml![G:\TPMercrediRX\Capture du 2022-07-03 12-53-56.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.053.png)**

D’après la capture ci-dessus le client **a 10.45.0.6** comme d adresse IP

Activation du routage et testons si l’ue parvient à accéder à la connexion internet

**#echo 1 > /proc/sys/net/ipv4/ip\_forward** 

**# sysctl –p**

**# iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o ogstun -j MASQUERADE** 

\# **iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o enp0s3 -j MASQUERADE**

#**ifconfig uesimtun0**

**#ping –I uesimtun0 8.8.8.8**

![G:\TPMercrediRX\Capture du 2022-07-03 13-19-42.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.054.png)

On peut aussi utiliser le navigateur firefox du client2 pour tester la connexion internet 

Pour ouvrir le navigateur fixefox on tape la commande suivante 

**#./nr-binder 10.45.0.6 firefox** 

![G:\TPMercrediRX\Capture du 2022-07-03 13-21-10.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.055.png)

Essayons de consulter le site de EC2LT à partir du navigateur de client2

![G:\TPMercrediRX\Capture du 2022-07-03 13-24-47.png](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.056.png)

**Conclusion**

Pour ce TP, nous avons utilisé les logiciels open source Open5gs qui simule le cœur du réseau et le UERANSIM qui simule le UE et la partie Radio (gNodeB)  
23

**ECOLE CENTRALE DES LOGICIELS LIBRES ET TELECOMMUNICATIONS** 
![](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.057.png)![](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.058.png)![](Aspose.Words.9a6623cc-f2eb-4cca-ade7-cd2016fea195.059.png)
